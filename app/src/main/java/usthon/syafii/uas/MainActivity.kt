package usthon.syafii.uas

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() , View.OnClickListener{

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        btnResep.setOnClickListener(this)
        btnKat.setOnClickListener(this)
        btnAlat.setOnClickListener(this)
    }

    override fun onClick(v: View?) {
        when(v?.id){
            R.id.btnResep ->{
                var resep = Intent(this, ResepActivity::class.java)
                startActivity(resep)
                true
            }
            R.id.btnKat ->{
                var kat = Intent(this, KatActivity::class.java)
                startActivity(kat)
                true
            }
            R.id.btnAlat -> {
                var alat = Intent(this, AlatActivity::class.java)
                startActivity(alat)
                true
            }
        }
    }
}