package usthon.syafii.uas

import android.content.Context
import android.content.Intent
import android.content.SharedPreferences
import android.graphics.Color
import android.os.Bundle
import android.view.Menu
import android.view.MenuItem
import android.view.View
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.recyclerview.widget.LinearLayoutManager
import com.android.volley.Request
import com.android.volley.Response
import com.android.volley.toolbox.StringRequest
import com.android.volley.toolbox.Volley
import kotlinx.android.synthetic.main.activity_kat.*
import org.json.JSONArray
import org.json.JSONObject

class KatActivity() : AppCompatActivity(), View.OnClickListener {
    lateinit var preferences : SharedPreferences
    val PREF_NAME = "setting"
    var BG_COLOR = "bg_color"
    var SUBTITLE_FONT_SIZE = "head_font_size"
    val DEF_BG_COLOR = "WHITE"
    val DEF_SUBTITLE_FONT_SIZE = 12
    lateinit var katAdapter: AdapterKat
    var daftarKat = mutableListOf<HashMap<String,String>>()
    var idKat = ""
    val mainUrl = "http://192.168.43.62/resep/"
    val url = mainUrl+"show_kat.php"
    val url2 = mainUrl+"query_kat.php"

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_kat)
        katAdapter = AdapterKat(daftarKat,this)
        listKat.layoutManager = LinearLayoutManager(this)
        listKat.adapter = katAdapter
        btnInsert.setOnClickListener(this)
        btnUpdate.setOnClickListener(this)
        btnDelete.setOnClickListener(this)
        btnFind.setOnClickListener(this)
    }

    fun showDataKat(namaKat : String){
        val request = object : StringRequest(
            Request.Method.POST,url,
            Response.Listener { response ->
                daftarKat.clear()
                val jsonArray = JSONArray(response)
                for(x in 0 .. (jsonArray.length()-1)){
                    val jsonObject = jsonArray.getJSONObject(x)
                    var kat = HashMap<String,String>()
                    kat.put("id_kat",jsonObject.getString("id_kat"))
                    kat.put("nama_kat",jsonObject.getString("nama_kat"))
                    daftarKat.add(kat)
                }
                katAdapter.notifyDataSetChanged()
            },
            Response.ErrorListener { error ->
                Toast.makeText(this,"Terjadi kesalahan koneksi ke server", Toast.LENGTH_LONG).show()
            }){
            override fun getParams(): MutableMap<String, String>{
                val hm = HashMap<String, String>()
                hm.put("nama_kat",namaKat)
                return hm
            }
        }
        val queue = Volley.newRequestQueue(this)
        queue.add(request)
    }

    fun queryKat(mode : String){
        val request = object : StringRequest(
            Method.POST,url2,
            Response.Listener { response ->
                val jsonObject = JSONObject(response)
                val error = jsonObject.getString("kode")
                if(error.equals("000")){
                    Toast.makeText(this,"Operasi Berhasil", Toast.LENGTH_LONG).show()
                    showDataKat("")
                    clearInput()
                }else{
                    Toast.makeText(this,"Operasi Gagal", Toast.LENGTH_LONG).show()
                }
            },
            Response.ErrorListener { error ->
                Toast.makeText(this,"Tidak dapat terhubung ke server", Toast.LENGTH_LONG).show()
            }){
            override fun getParams(): MutableMap<String, String> {
                val hm = HashMap<String,String>()
                when(mode){
                    "update" -> {
                        hm.put("mode","update")
                        hm.put("id_kat",idKat)
                        hm.put("nama_kat",edNamaKat.text.toString())
                    }
                    "insert" -> {
                        hm.put("mode","insert")
                        hm.put("nama_kat",edNamaKat.text.toString())
                    }
                    "delete" -> {
                        hm.put("mode","delete")
                        hm.put("id_kat",idKat)
                    }
                }
                return hm
            }
        }
        val queue = Volley.newRequestQueue(this)
        queue.add(request)
    }

    override fun onClick(v: View?) {
        when(v?.id){
            R.id.btnInsert ->{
                queryKat("insert")
            }
            R.id.btnUpdate ->{
                queryKat("update")
            }
            R.id.btnDelete ->{
                queryKat("delete")
            }
            R.id.btnFind -> {
                showDataKat(edNamaKat.text.toString())
            }
        }
    }
    fun loadSetting() {
        preferences = getSharedPreferences(PREF_NAME, Context.MODE_PRIVATE)
        val Layout = findViewById(R.id.cLayout) as ConstraintLayout
        Layout.setBackgroundColor(Color.parseColor(preferences.getString(BG_COLOR,DEF_BG_COLOR)))
        txTitle.textSize = preferences.getInt(SUBTITLE_FONT_SIZE,DEF_SUBTITLE_FONT_SIZE).toFloat()
    }

    override fun onStart() {
        super.onStart()
        showDataKat("")
        loadSetting()
    }

    fun clearInput(){
        idKat = ""
        edNamaKat.setText("")
    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        var mnuInflater = menuInflater
        mnuInflater.inflate(R.menu.option_menu,menu)
        return super.onCreateOptionsMenu(menu)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when(item?.itemId){
            R.id.itemSetting ->{
                var intent = Intent(this,SettingActivity::class.java)
                startActivity(intent)
            }
        }
        return super.onOptionsItemSelected(item)
    }


}