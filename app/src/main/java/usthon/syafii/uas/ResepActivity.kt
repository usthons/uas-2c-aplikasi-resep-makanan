package usthon.syafii.uas

import android.app.Activity
import android.content.Context
import android.content.Intent
import android.content.SharedPreferences
import android.graphics.Color
import android.os.Bundle
import android.view.Menu
import android.view.MenuItem
import android.view.View
import android.widget.AdapterView
import android.widget.ArrayAdapter
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.recyclerview.widget.LinearLayoutManager
import com.android.volley.Request
import com.android.volley.Response
import com.android.volley.toolbox.StringRequest
import com.android.volley.toolbox.Volley
import kotlinx.android.synthetic.main.activity_kat.*
import kotlinx.android.synthetic.main.activity_resep.*
import kotlinx.android.synthetic.main.activity_resep.btnDelete
import kotlinx.android.synthetic.main.activity_resep.btnFind
import kotlinx.android.synthetic.main.activity_resep.btnInsert
import kotlinx.android.synthetic.main.activity_resep.btnUpdate
import kotlinx.android.synthetic.main.activity_resep.txTitle
import org.json.JSONArray
import org.json.JSONObject
import usthon.syafii.uas.MediaHelper
import usthon.syafii.uas.R
import java.text.SimpleDateFormat
import java.util.*
import kotlin.collections.HashMap

class ResepActivity : AppCompatActivity(), View.OnClickListener {
    lateinit var preferences : SharedPreferences
    val PREF_NAME = "setting"
    var BG_COLOR = "bg_color"
    var SUBTITLE_FONT_SIZE = "head_font_size"
    val DEF_BG_COLOR = "WHITE"
    val DEF_SUBTITLE_FONT_SIZE = 12
    lateinit var mediaHelper : MediaHelper
    lateinit var resepAdapter: AdapterResep
    lateinit var katAdapter : ArrayAdapter<String>
    var daftarResep = mutableListOf<HashMap<String,String>>()
    var daftarKat = mutableListOf<String>()
    val mainUrl = "http://192.168.43.62/resep/"
    val url = mainUrl+"show_resep.php"
    var url2 = mainUrl+"show_kat.php"
    var url3 = mainUrl+"query_resep.php"
    var imStr = ""
    var pilihKat = ""

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_resep)
        resepAdapter = AdapterResep(daftarResep,this)
        mediaHelper = MediaHelper(this)
        listResep.layoutManager = LinearLayoutManager(this)
        listResep.adapter = resepAdapter

        katAdapter = ArrayAdapter(this,android.R.layout.simple_dropdown_item_1line,
            daftarKat)
        spKat.adapter = katAdapter
        spKat.onItemSelectedListener = itemSelected

        imgUpload.setOnClickListener(this)
        btnUpdate.setOnClickListener(this)
        btnInsert.setOnClickListener(this)
        btnDelete.setOnClickListener(this)
        btnFind.setOnClickListener(this)
    }

    override fun onClick(v: View?) {
        when(v?.id){
            R.id.imgUpload -> {
                val intent = Intent()
                intent.setType("image/*")
                intent.setAction(Intent.ACTION_GET_CONTENT)
                startActivityForResult(intent,mediaHelper.getRcGallery())
            }
            R.id.btnInsert -> {
                queryresep("insert")
            }
            R.id.btnDelete -> {
                queryresep("delete")
            }
            R.id.btnUpdate -> {
                queryresep("update")
            }
            R.id.btnFind -> {
                showDataResep(edNama.text.toString().trim())
            }
        }
    }
    val itemSelected = object : AdapterView.OnItemSelectedListener {
        override fun onNothingSelected(parent: AdapterView<*>?) {
            spKat.setSelection(0)
            pilihKat = daftarKat.get(0)
        }

        override fun onItemSelected(parent: AdapterView<*>?, view: View?, position: Int, id: Long) {
            pilihKat = daftarKat.get(position)
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if(resultCode == Activity.RESULT_OK){
            if(requestCode == mediaHelper.getRcGallery()){
                imStr = mediaHelper.getBitmapToString(data!!.data,imgUpload)
            }
        }
    }

    fun queryresep(mode : String){
        val request = object : StringRequest(
            Method.POST,url3,
            Response.Listener { response ->
                val jsonObject = JSONObject(response)
                val error = jsonObject.getString("kode")
                if(error.equals("000")){
                    Toast.makeText(this,"Operasi Berhasil", Toast.LENGTH_LONG).show()
                    showDataResep("")
                }else{
                    Toast.makeText(this,"Operasi Gagal", Toast.LENGTH_LONG).show()
                }
            },
            Response.ErrorListener { error ->
                Toast.makeText(this,"Tidak dapat terhubung ke server", Toast.LENGTH_LONG).show()
            }){
            override fun getParams(): MutableMap<String, String> {
                val hm = HashMap<String,String>()
                val nmFile = "DC"+ SimpleDateFormat("yyyyMMddHHmmss", Locale.getDefault())
                    .format(Date())+".jpg"
                when(mode){
                    "insert" -> {
                        hm.put("mode","insert")
                        hm.put("kode",edResep.text.toString())
                        hm.put("nama",edNama.text.toString())
                        hm.put("bahan1",edBahan1.text.toString())
                        hm.put("bahan2",edBahan2.text.toString())
                        hm.put("bahan3",edBahan3.text.toString())
                        hm.put("image",imStr)
                        hm.put("file",nmFile)
                        hm.put("nama_kat",pilihKat)
                    }
                    "update" -> {
                        hm.put("mode","update")
                        hm.put("kode",edResep.text.toString())
                        hm.put("nama",edNama.text.toString())
                        hm.put("bahan1",edBahan1.text.toString())
                        hm.put("bahan2",edBahan2.text.toString())
                        hm.put("bahan3",edBahan3.text.toString())
                        hm.put("image",imStr)
                        hm.put("file",nmFile)
                        hm.put("nama_kat",pilihKat)
                    }
                    "delete" -> {
                        hm.put("mode","delete")
                        hm.put("kode",edResep.text.toString())
                    }
                }
                return hm
            }
        }
        val queue = Volley.newRequestQueue(this)
        queue.add(request)
    }

    fun getNamaKat(namaKat : String){
        val request = object :StringRequest(
            Request.Method.POST,url2,
            Response.Listener { response ->
                daftarKat.clear()
                val jsonArray = JSONArray(response)
                for(x in 0 .. (jsonArray.length()-1)){
                    val jsonObject = jsonArray.getJSONObject(x)
                    daftarKat.add(jsonObject.getString("nama_kat"))
                }
                katAdapter.notifyDataSetChanged()
            },
            Response.ErrorListener { error ->
                Toast.makeText(this,"Terjadi kesalahan koneksi ke server",Toast.LENGTH_LONG).show()
            }){
            override fun getParams(): MutableMap<String, String>{
                val hm = HashMap<String, String>()
                hm.put("nama_kat",namaKat)
                return hm
            }
        }
        val queue = Volley.newRequestQueue(this)
        queue.add(request)
    }

    fun showDataResep(namaResep : String){
        val request = object :StringRequest(
            Request.Method.POST,url,
            Response.Listener { response ->
                daftarResep.clear()
                val jsonArray = JSONArray(response)
                for(x in 0 .. (jsonArray.length()-1)){
                    val jsonObject = jsonArray.getJSONObject(x)
                    var resep = HashMap<String,String>()
                    resep.put("kode",jsonObject.getString("kode"))
                    resep.put("nama",jsonObject.getString("nama"))
                    resep.put("bahan1",jsonObject.getString("bahan1"))
                    resep.put("bahan2",jsonObject.getString("bahan2"))
                    resep.put("bahan3",jsonObject.getString("bahan3"))
                    resep.put("nama_kat",jsonObject.getString("nama_kat"))
                    resep.put("url",jsonObject.getString("url"))
                    daftarResep.add(resep)
                }
                resepAdapter.notifyDataSetChanged()
            },
            Response.ErrorListener { error ->
                Toast.makeText(this,"Terjadi kesalahan koneksi ke server",Toast.LENGTH_LONG).show()
            }){
            override fun getParams(): MutableMap<String, String>{
                val hm = HashMap<String, String>()
                hm.put("nama",namaResep)
                return hm
            }
        }
        val queue = Volley.newRequestQueue(this)
        queue.add(request)
    }

    fun loadSetting() {
        preferences = getSharedPreferences(PREF_NAME, Context.MODE_PRIVATE)
        val Layout = findViewById(R.id.cLayout) as ConstraintLayout
        Layout.setBackgroundColor(Color.parseColor(preferences.getString(BG_COLOR,DEF_BG_COLOR)))
        txTitle.textSize = preferences.getInt(SUBTITLE_FONT_SIZE,DEF_SUBTITLE_FONT_SIZE).toFloat()
    }
    override fun onStart() {
        super.onStart()
        showDataResep("")
        getNamaKat("")
        loadSetting()
    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        var mnuInflater = menuInflater
        mnuInflater.inflate(R.menu.option_menu,menu)
        return super.onCreateOptionsMenu(menu)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when(item?.itemId){
            R.id.itemSetting ->{
                var intent = Intent(this,SettingActivity::class.java)
                startActivity(intent)
            }
        }
        return super.onOptionsItemSelected(item)
    }
}