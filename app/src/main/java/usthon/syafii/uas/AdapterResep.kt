package usthon.syafii.uas

import android.graphics.Color
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.recyclerview.widget.RecyclerView
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.activity_resep.*
import usthon.syafii.uas.R

class AdapterResep(
    val dataResep: List<HashMap<String, String>>,
    val resepActivity: ResepActivity):
    RecyclerView.Adapter<AdapterResep.HolderDataResep>() {
    override fun onCreateViewHolder(p0: ViewGroup, p1: Int): AdapterResep.HolderDataResep {
        val v = LayoutInflater.from(p0.context).inflate(R.layout.row_resep,p0,false)
        return  HolderDataResep(v)
    }

    override fun getItemCount(): Int {
        return dataResep.size
    }

    override fun onBindViewHolder(p0 : AdapterResep.HolderDataResep, p1: Int) {
        val data = dataResep.get(p1)
        p0.txtKode.setText(data.get("kode"))
        p0.txtNama.setText(data.get("nama"))
        p0.txtBahan1.setText(data.get("bahan1"))
        p0.txtBahan2.setText(data.get("bahan2"))
        p0.txtBahan3.setText(data.get("bahan3"))
        p0.txtKat.setText(data.get("nama_kat"))

        if(p1.rem(2) == 0) p0.cLayout.setBackgroundColor(
            Color.rgb(230,245,240))
        else p0.cLayout.setBackgroundColor(Color.rgb(255,255,245))

        p0.cLayout.setOnClickListener ({
            val pos = resepActivity.daftarKat.indexOf(data.get("nama_kat"))
            resepActivity.spKat.setSelection(pos)
            resepActivity.edResep.setText(data.get("kode"))
            resepActivity.edNama.setText(data.get("nama"))
            resepActivity.edBahan1.setText(data.get("bahan1"))
            resepActivity.edBahan2.setText(data.get("bahan2"))
            resepActivity.edBahan3.setText(data.get("bahan3"))
            Picasso.get().load(data.get("url")).into(resepActivity.imgUpload)

        })

        if(!data.get("url").equals(""))
            Picasso.get().load(data.get("url")).into(p0.photo)
    }

    class HolderDataResep(v: View) : RecyclerView.ViewHolder(v){
        val txtKode = v.findViewById<TextView>(R.id.txtKode)
        val txtNama = v.findViewById<TextView>(R.id.txtNama)
        val txtBahan1 = v.findViewById<TextView>(R.id.txtBahan1)
        val txtBahan2 = v.findViewById<TextView>(R.id.txtBahan2)
        val txtBahan3 = v.findViewById<TextView>(R.id.txtBahan3)
        val txtKat = v.findViewById<TextView>(R.id.txtKat)
        val photo = v.findViewById<ImageView>(R.id.imgUpload)
        val cLayout = v.findViewById<ConstraintLayout>(R.id.cLayout)
    }

}
