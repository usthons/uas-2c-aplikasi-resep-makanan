package usthon.syafii.uas

import android.graphics.Color
import android.net.Uri
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.recyclerview.widget.RecyclerView
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.activity_alat.*

class AdapterAlat(val dataAlat : List<HashMap<String,String>>, val alatActivity: AlatActivity) :
    RecyclerView.Adapter<AdapterAlat.HolderDataAlat>() {
    override fun onCreateViewHolder(p0: ViewGroup, p1: Int): AdapterAlat.HolderDataAlat {
        val v = LayoutInflater.from(p0.context).inflate(R.layout.row_alat, p0, false)
        return AdapterAlat.HolderDataAlat(v)
    }

    override fun getItemCount(): Int {
        return dataAlat.size
    }

    override fun onBindViewHolder(p0 : AdapterAlat.HolderDataAlat, p1: Int) {
        val data = dataAlat.get(p1)
        p0.txtKodeAlat.setText(data.get("kode_alat"))
        p0.txtNamaAlat.setText(data.get("nama_alat"))
        p0.photo.setImageURI(Uri.parse(data.get("url")))

        if(!data.get("url").equals(""))
            Picasso.get().load(data.get("url")).into(p0.photo)
    }

    class HolderDataAlat(v: View) : RecyclerView.ViewHolder(v) {
        val txtKodeAlat = v.findViewById<TextView>(R.id.txtKodeAlat)
        val txtNamaAlat = v.findViewById<TextView>(R.id.txtNamaAlat)
        val photo = v.findViewById<ImageView>(R.id.imgUpload)
    }
}