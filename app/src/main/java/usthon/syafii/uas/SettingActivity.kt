package usthon.syafii.uas

import android.app.Activity
import android.content.Context
import android.content.Intent
import android.content.SharedPreferences
import android.os.Bundle
import android.view.View
import android.widget.AdapterView
import android.widget.ArrayAdapter
import android.widget.SeekBar
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import kotlinx.android.synthetic.main.activity_setting.*

class SettingActivity : AppCompatActivity(), View.OnClickListener, SeekBar.OnSeekBarChangeListener{

    lateinit var preferences : SharedPreferences
    val PREF_NAME = "setting"
    var BG_COLOR = "bg_color"
    var SUBTITLE_FONT_SIZE = "head_font_size"
    val DEF_BG_COLOR = "WHITE"
    val DEF_SUBTITLE_FONT_SIZE = 12
    var BGColor : String = ""
    val arrayBGColor = arrayOf("BLUE","YELLOW","GREEN","BLACK")
    lateinit var adapterSpin : ArrayAdapter<String>

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_setting)
        //listener
        btnSimpan.setOnClickListener(this)
        adapterSpin = ArrayAdapter(this,android.R.layout.simple_list_item_1,arrayBGColor)
        spBGColor.adapter = adapterSpin
        spBGColor.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
            override fun onNothingSelected(parent: AdapterView<*>?) {
            }
            override fun onItemSelected(
                parent: AdapterView<*>?,
                view: View?,
                position: Int,
                id: Long
            ) {
                BGColor = adapterSpin.getItem(position).toString()
            }
        }
        //get resources from SharedPreferences
        preferences = getSharedPreferences(PREF_NAME, Context.MODE_PRIVATE)
        var spinnerselection = adapterSpin.getPosition(preferences.getString(BG_COLOR,DEF_BG_COLOR))
        spBGColor.setSelection(spinnerselection,true)
        sbFSubTitle.progress = preferences.getInt(SUBTITLE_FONT_SIZE,DEF_SUBTITLE_FONT_SIZE)
        sbFSubTitle.setOnSeekBarChangeListener(this)
    }

    override fun onClick(v: View?) {
        when(v?.id){
            R.id.btnSimpan ->{
                //save configuration to SharedPreferences
                preferences = getSharedPreferences(PREF_NAME, Context.MODE_PRIVATE)
                val prefEditor = preferences.edit()
                prefEditor.putString(BG_COLOR,BGColor)
                prefEditor.putInt(SUBTITLE_FONT_SIZE,sbFSubTitle.progress)
                prefEditor.commit()
                Toast.makeText(this,"Perubahan Disimpan",Toast.LENGTH_SHORT).show()
            }
        }
    }

    override fun onProgressChanged(seekBar: SeekBar?, progress: Int, fromUser: Boolean) {
        when(seekBar?.id){
            R.id.sbFSubTitle ->{

            }
        }
    }

    override fun onStartTrackingTouch(seekBar: SeekBar?) {

    }

    override fun onStopTrackingTouch(seekBar: SeekBar?) {

    }
}