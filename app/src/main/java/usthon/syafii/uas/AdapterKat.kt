package usthon.syafii.uas

import android.graphics.Color
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.recyclerview.widget.RecyclerView
import kotlinx.android.synthetic.main.activity_kat.*

class AdapterKat(val dataKat : List<HashMap<String,String>>, val katActivity: KatActivity) :
    RecyclerView.Adapter<AdapterKat.HolderDataKat>() {
    override fun onCreateViewHolder(p0: ViewGroup, p1: Int): AdapterKat.HolderDataKat {
        val v = LayoutInflater.from(p0.context).inflate(R.layout.row_kat, p0, false)
        return HolderDataKat(v)
    }

    override fun getItemCount(): Int {
        return dataKat.size
    }

    override fun onBindViewHolder(holder: AdapterKat.HolderDataKat, position: Int) {
        val data = dataKat.get(position)
        holder.txtNamaKat.setText(data.get("nama_kat"))
        if(position.rem(2) == 0) holder.cLayout.setBackgroundColor(
            Color.rgb(230,245,240))
        else holder.cLayout.setBackgroundColor(Color.rgb(255,255,245))

        holder.cLayout.setOnClickListener({
            katActivity.idKat = data.get("id_kat").toString()
            katActivity.edNamaKat.setText(data.get("nama_kat"))
        })
    }

    class HolderDataKat(v: View) : RecyclerView.ViewHolder(v) {
        val txtNamaKat = v.findViewById<TextView>(R.id.txtNamaKat)
        val cLayout = v.findViewById<ConstraintLayout>(R.id.cLayout)
    }
}