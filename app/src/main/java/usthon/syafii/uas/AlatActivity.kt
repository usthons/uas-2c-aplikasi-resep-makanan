package usthon.syafii.uas

import android.app.Activity
import android.content.Context
import android.content.Intent
import android.content.SharedPreferences
import android.graphics.Color
import android.os.Bundle
import android.view.Menu
import android.view.MenuItem
import android.view.View
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.LinearLayoutManager
import com.android.volley.Request
import com.android.volley.Response
import com.android.volley.toolbox.StringRequest
import com.android.volley.toolbox.Volley
import kotlinx.android.synthetic.main.activity_alat.*
import org.json.JSONArray
import org.json.JSONObject
import android.widget.ArrayAdapter
import androidx.constraintlayout.widget.ConstraintLayout
import kotlinx.android.synthetic.main.activity_alat.txTitle
import kotlinx.android.synthetic.main.activity_kat.*
import java.text.SimpleDateFormat
import java.util.*
import kotlin.collections.HashMap

class AlatActivity() : AppCompatActivity(), View.OnClickListener {
    lateinit var preferences : SharedPreferences
    val PREF_NAME = "setting"
    var BG_COLOR = "bg_color"
    var SUBTITLE_FONT_SIZE = "head_font_size"
    val DEF_BG_COLOR = "WHITE"
    val DEF_SUBTITLE_FONT_SIZE = 12
    lateinit var mediaHelper : MediaHelper
    lateinit var alatAdapter: AdapterAlat
    var daftarAlat = mutableListOf<HashMap<String,String>>()
    val mainUrl = "http://192.168.43.62/resep/"
    val url = mainUrl+"show_alat.php"
    var imStr = ""

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_alat)
        alatAdapter = AdapterAlat(daftarAlat,this)
        mediaHelper = MediaHelper(this)
        listAlat.layoutManager = LinearLayoutManager(this)
        listAlat.adapter = alatAdapter

    }

    override fun onClick(v: View?) {
        when(v?.id){
            R.id.imgUpload -> {
            }
            R.id.btnInsert -> {
            }
            R.id.btnDelete -> {
            }
            R.id.btnUpdate -> {
            }
            R.id.btnFind -> {
            }
        }
    }


    fun showDataAlat(namaAlat : String){
        val request = object : StringRequest(
            Request.Method.POST,url,
            Response.Listener { response ->
                daftarAlat.clear()
                val jsonArray = JSONArray(response)
                for(x in 0 .. (jsonArray.length()-1)){
                    val jsonObject = jsonArray.getJSONObject(x)
                    var alat = HashMap<String,String>()
                    alat.put("kode_alat",jsonObject.getString("kode_alat"))
                    alat.put("nama_alat",jsonObject.getString("nama_alat"))
                    alat.put("url",jsonObject.getString("url"))
                    daftarAlat.add(alat)
                }
                alatAdapter.notifyDataSetChanged()
            },
            Response.ErrorListener { error ->
                Toast.makeText(this,"Terjadi kesalahan koneksi ke server", Toast.LENGTH_LONG).show()
            }){
            override fun getParams(): MutableMap<String, String>{
                val hm = HashMap<String, String>()
                hm.put("nama_alat",namaAlat)
                return hm
            }
        }
        val queue = Volley.newRequestQueue(this)
        queue.add(request)
    }

    fun loadSetting() {
        preferences = getSharedPreferences(PREF_NAME, Context.MODE_PRIVATE)
        val Layout = findViewById(R.id.cLayout) as ConstraintLayout
        Layout.setBackgroundColor(Color.parseColor(preferences.getString(BG_COLOR,DEF_BG_COLOR)))
        txTitle.textSize = preferences.getInt(SUBTITLE_FONT_SIZE,DEF_SUBTITLE_FONT_SIZE).toFloat()
    }

    override fun onStart() {
        super.onStart()
        showDataAlat("")
        loadSetting()
    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        var mnuInflater = menuInflater
        mnuInflater.inflate(R.menu.option_menu,menu)
        return super.onCreateOptionsMenu(menu)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when(item?.itemId){
            R.id.itemSetting ->{
                var intent = Intent(this,SettingActivity::class.java)
                startActivity(intent)
            }
        }
        return super.onOptionsItemSelected(item)
    }

}